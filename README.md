# Web Services with Spring
Boilerplate Spring Web Services

Stack:
 - java 8
 - maven 3.3
 - docker 1.9


## How to run application & execute integration tests
### With Docker
**Prerequisites**
 - Follow [instructions](https://github.com/tecris/docker/blob/v3.3/nexus/README.md) start nexus and add jboss repository (as proxy repository) to nexus

**One liner**

 - `$ mvn clean integration-test -Pcontinuous-delivery -Dmaven.buildNumber.doCheck=false`

**Step-by-step**
  ```
  $ docker run -d --name ws-spring-demo -p 8080:8080 -p 9990:9990 casadocker/wildfly-soap:10.0.0    # start wildfly container
  $ mvn clean wildfly:deploy                                                                        # deploy application
  $ mvn clean integration-test                                                                      # run integration tests
  $ mvn clean wildfly:undeploy                                                                      # undeploy application (optional)
  ```

### With Spring Boot
  ```
  $ mvn clean package
  $ java -jar target/ROOT.war
  $ mvn clean integration-test
  ```

### Manual test deployment:
 - `$ ./postRequest.sh`

### WSDL
 - `http://localhost:8080/ws/scoreboard.wsdl`
